#! /bin/sh

set -e

OUTPUT_HTML=$1
CLIENT_JS=$2
CSS=$3
DEBUG_LEVEL=$4
TITLE=$5
DIV_ID=$6

cat <<EOBLOB > $OUTPUT_HTML
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
EOBLOB
echo "<!-- Generated on `date` -->" >> $OUTPUT_HTML
cat <<EOBLOB >> $OUTPUT_HTML
    <style>
EOBLOB
cat $CSS >> $OUTPUT_HTML
cat <<EOBLOB >> $OUTPUT_HTML

ul.inline-items-separated {
    padding: 0;
    margin: 2px;
}
.inline-items-separated>li {
    padding: 0;
    margin: 0;
    padding-left: 0.2em;
    padding-right: 0.2em;
}
    </style>
    <title>$TITLE</title>
  </head>
  <body>
    <div class="container-fluid" id="$DIV_ID"></div>
    <script>
      window.gui_debug_level = "$DEBUG_LEVEL";
      window.template_assembly_time = "$(date)";
EOBLOB
cat $CLIENT_JS >> $OUTPUT_HTML
cat <<EOBLOB >> $OUTPUT_HTML
</script></body></html>
EOBLOB
