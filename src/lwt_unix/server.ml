open Printf
let debug fmt = ksprintf (eprintf "debug: %s\n%!") fmt

let failwithf fmt = ksprintf Lwt.fail_with fmt
    
(** A common error that simply means “invalid argument”. *)
let wrong_request fmt = ksprintf (fun s -> failwithf "wrong_request: %s" s) fmt

(** Check that it is a [`POST], get the {i non-empty} body; or fail. *)
let get_post_body request ~body =
  begin match Cohttp.Request.meth request with
  | `POST ->
    debug  "It is a GET request";
    Cohttp_lwt_body.to_string body
  | other ->
    wrong_request "wrong method %S" (Cohttp.Code.string_of_method other)
  end

module Server_state = struct
  type t = {
    sessions : (string * unit) list;
  }
  let create () = {sessions = []}

end

let answer_api_message ~state msg =
  let open Lwt in
  begin match msg with
  | `Get_server_info ->
    let open Ocoty.Protocol.Down_message_v0 in
    let time = Unix.gettimeofday () in
    let tls =
      match !Conduit_lwt_unix.tls_library with
      | Conduit_lwt_unix.OpenSSL  -> `OpenSSL
      | Conduit_lwt_unix.Native  -> `Native
      | Conduit_lwt_unix.No_tls -> `None
    in
    let info = {time; tls} in
    return (`Server_info info)
  end

let handle_request ~state ~body ~request () =
  let open Lwt in
  debug "Request-in: %s"
    (Cohttp.Request.sexp_of_t request |> Sexplib.Sexp.to_string);
  match Uri.path (Cohttp.Request.uri request) with
  | "/hello" -> return `Unit
  | "/api" ->
    get_post_body request ~body
    >>= fun body ->
    begin try return (Ocoty.Protocol.Up_message.deserialize_exn body) with
    | e ->
      wrong_request "wrong up message: %s" body
    end
    >>= fun up_msg ->
    answer_api_message ~state up_msg
    >>= fun down_msg ->
    return (`String (Ocoty.Protocol.Down_message.serialize down_msg))
  | "/" ->
    return (`String (Client_html.gui_page))
  | other -> wrong_request "unknown path %S" other



let start ~port ?tls () =
  let server_state = Server_state.create () in
  let mode =
    match tls with
    | Some (certfile, keyfile) ->
      `TLS (
        (* `TLS means that conduit will check the env-var "CONDUIT_TLS" 
           ( "native" | "Native" | "NATIVE" -> Native
               | _ -> OpenSSL) *)
        `Crt_file_path certfile,
        `Key_file_path keyfile,
        `No_password, `Port port)
    | None -> `TCP (`Port port)
  in
  let server_callback (_, conn_id) request body =
    let open Lwt in
    let id = Cohttp.Connection.to_string conn_id in
    debug "incoming-connection: %s" id;
    let open Lwt in
    catch
      (fun () ->
         handle_request ~state:server_state ~body ~request  ()
         >>= fun ok ->
         return (`Ok ok))
      (fun exn -> return (`Error (Printexc.to_string exn)))
    >>= fun high_level_answer ->
    begin match high_level_answer with
    | `Ok `Unit ->
      Cohttp_lwt_unix.Server.respond_string ~status:`OK  ~body:"" ()
    | `Ok (`String body) ->
      Cohttp_lwt_unix.Server.respond_string ~status:`OK  ~body ()
    | `Error body ->
      Cohttp_lwt_unix.Server.respond_string ~status:`Not_found  ~body ()
    end
  in
  let conn_closed (_, conn_id) =
    debug  "conn %S closed" (Cohttp.Connection.to_string conn_id) in
  Cohttp_lwt_unix.Server.(
    create ~mode (make ~callback:server_callback ~conn_closed ()))
