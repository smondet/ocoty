let usage_and_exit () =
  Printf.eprintf "usage: \n\
                 \       %s <port>\n\
                 \       %s <port> <tls-cert> <tls-key>\n%!"
    Sys.argv.(0)
    Sys.argv.(0);
  exit 1

let () =
  let port_of_string s = try int_of_string s with _ -> usage_and_exit () in 
  match Sys.argv |> Array.to_list |> List.tl with
  | [] -> usage_and_exit ()
  | one :: [] ->
    let port = port_of_string one in
    Ocoty_lwt_unix.Server.start ~port () |> Lwt_main.run
  | one :: cert :: key :: [] ->
    let port = port_of_string one in
    Ocoty_lwt_unix.Server.start ~port ~tls:(cert, key) () |> Lwt_main.run
  | other -> usage_and_exit ()
