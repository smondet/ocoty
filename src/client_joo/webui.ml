
open Ocoty
(* We fully use the `ocoty` library on the client side as well. *)

(* `debug` prints on the Javacsript console. *)
let debug f =
  Printf.ksprintf (fun s -> Firebug.console##log(Js.string s)) f
let lwt_failf fmt = Printf.ksprintf Lwt.fail_with fmt

let now () = Unix.gettimeofday ()
(* this function from `unix` has been reimplemented by `js_of_ocaml` library *)

module H5 = struct
  (* the module H5 provides mainly Html5, then there is a sub-module
     `Reactive_node` to create, well, reactive nodes. *)
  include Tyxml_js.Html5
  module Reactive_node = Tyxml_js.R.Html5

  let to_dom e = Tyxml_js.To_dom.of_node e

  (* A link (`<a .. > ... </a>`) with “local” behavior by default (the
     `on_click` function) *)
  let local_anchor ~on_click ?(a=[]) content =
    Tyxml_js.Html5.a ~a:(
      [
        a_href "javascript:;";
        (* The `href` makes the brwoser display the mouse pointer like
           a proper link.
           Cf. http://stackoverflow.com/questions/5637969/is-an-empty-href-valid *)
        a_onclick on_click;
      ]
      @ a)
      content
end

(* A module simplifiying the creation of `'a ReactiveData.RList.t`
   values from `_ React.S.t` signals. Most functions in
   `H5.Reactive_node` expect such lists. *)
module Reactive_list: sig
  type 'a t = 'a ReactiveData.RList.t
  val singleton: 'a React.S.t -> 'a t  (* a list with one element *)
  val make: 'a list React.S.t -> 'a t  (* a list signal → a reactive list *)
end = struct
  type 'a t = 'a ReactiveData.RList.t
  let singleton t =
    let open ReactiveData.RList in
    from_event
      [React.S.value t]
      (React.E.map (fun e -> Set [e]) (React.S.changes t))
  let make t =
    let open ReactiveData.RList in
    from_event
      (React.S.value t)
      (React.E.map (fun e -> Set e) (React.S.changes t))
end

(* The module `XmlHttpRequest` does not provide a high-level function
   for “raw body” POST requests (only for “form” ones). So we implement
   that `post` function going back to the JS obect. *)
module Custom_xml_http_request = struct
  let post url ~body =
    let task, condition = Lwt.task () in
    let open XmlHttpRequest in
    let xhr = create () in
    xhr##_open
      (Js.string "POST") (Js.string url) (Js._true);
    debug  "Custom_xml_http_request.post %s" url;
    xhr##.onreadystatechange :=  Js.wrap_callback
        (fun _ ->
           begin match xhr##.readyState with
           | DONE ->
             begin match xhr##.status with
             | 200 ->
               Lwt.wakeup condition (`Ok (Js.to_string xhr##.responseText))
             | other ->
               Lwt.wakeup condition
                 (`Error (`Wrong_xhr_status
                            (other, Js.to_string xhr##.statusText)))
             end
           | other -> debug  "READY other !! ";
           end);
    xhr##.ontimeout := Dom.handler (fun _ ->
        Lwt.wakeup condition (`Error (`Xhr_timeout));
        Js._false);
    xhr##send (Js.Opt.return (Js.string body));
    Lwt.on_cancel task (fun () -> xhr##abort);
    task
end

(* `Procotol_client.call_api` wraps an `Ocoty.Protocol.Up_message.t`
   in the `~body` of `Custom_xml_http_request.post` call and parses the
   output to an `Ocoty.Protocol.down_message.t`. *)
module Procotol_client = struct
  let call_api ?(timeout = 20.) msg =
    let open Lwt in
    Custom_xml_http_request.post "/api"
      ~body:(Protocol.Up_message.serialize msg)
    >>= begin function
    | `Ok str ->
      begin try
        return (Protocol.Down_message.deserialize_exn str)
      with e ->
        lwt_failf
          "Deserializing message\n%S\n%s"
          str
          (Printexc.to_string e)
      end
    | `Error (`Wrong_xhr_status (i, s)) ->
      lwt_failf "XHR wrong status (%d, %s)" i s
    | `Error `Xhr_timeout ->
      lwt_failf "XHR timeout"
    end
end

(* The module `Client_state` is all example code that uses two signals
   `React.S.t` and updates them by calling the server asynchronously.

   The function `render` shows how to display reactive signals.
 *)
module Client_state = struct
  type 'a signal_store = {
    signal: 'a React.S.t;
    set: 'a -> unit;
  }
  type t = {
    latest_news_from_server:
      Ocoty.Protocol.Down_message_v0.server_info option signal_store;
    notifications: (float * string) list signal_store;
  }

  let add_notification t msg =
    let notifs = React.S.value t.notifications.signal in
    t.notifications.set ((now (), msg) :: notifs);
    ()

  let get_server_info t =
    let open Lwt in
    Procotol_client.call_api `Get_server_info
    >>= fun answer ->
    begin match answer with
    | `Error s ->
      add_notification t ("Server-error: " ^ s);
      return ()
    | `Server_info info ->
      t.latest_news_from_server.set (Some info);
      return ()
    end

  let create () =
    let signal, set = React.S.create None in
    let latest_news_from_server = {signal; set} in
    let signal, set = React.S.create [] in
    let notifications = {signal; set} in
    let t = {latest_news_from_server; notifications} in
    Lwt.async (fun () -> get_server_info t);
    Lwt.async_exception_hook :=
      (fun exn ->
         add_notification t ("Local-exception: " ^ Printexc.to_string exn));
    t


  let date_to_string ?(style = `UTC) fl =
    let obj = new%js Js.date_fromTimeValue (1000. *. fl) in
    Js.to_string
      begin match style with
      | `ISO -> obj##toISOString
      | `Javascript -> obj##toString
      | `Locale -> obj##toLocaleString
      | `UTC -> obj##toUTCString
      end

  let render t =
    let open H5 in
    div [
      h3 [pcdata "Server Info"];
      Reactive_node.div React.(
          t.latest_news_from_server.signal
          |> S.map (function
            | None -> [pcdata "In progress …"]
            | Some info ->
              let open Ocoty.Protocol.Down_message_v0 in
              [
                pcdata (
                  Printf.sprintf
                    "On %s, the server's TLS implementation was %s"
                    (date_to_string info.time)
                    (match info.tls with
                    | `OpenSSL -> "OpenSSL"
                    | `None -> "missing"
                    | `Native -> "NQSB-TLS a.k.a. OCamlTLS")
                );
                pcdata " (";
                local_anchor [pcdata "reload"]
                  ~on_click:(fun _ ->
                      t.latest_news_from_server.set None;
                      Lwt.async (fun () -> get_server_info t);
                      true);
                pcdata ")";
              ]
            )
          |> Reactive_list.make
        );
      h3 [pcdata "Notifications"];
      Reactive_node.div React.(
          t.notifications.signal
          |> S.map (
            function
            | [] -> pcdata "No notifications"
            | more ->
              ul (List.rev_map
                    (fun (date, msg) ->
                       li [
                         pcdata (
                           Printf.sprintf
                             "On %s: %s" (date_to_string date) msg)
                       ]
                    )
                    more)
          )
          |> Reactive_list.singleton
        );
    ]
end

(* `attach_to_page` takes an `H5.t` (i.e. `Tyxml_js.Html5._`) and does
   the low-level JS plumbing to attach it to the rest of HTML page
   (thanks to `<div id="ocoty-gui"/>`).
*)
let attach_to_page gui =
  let base_div =
    Dom_html.getElementById "ocoty-gui" in
  base_div##appendChild (H5.to_dom gui) |> ignore;
  Lwt.return ()

(* `go` is the handler of the JS event `window.onload` *)
let go _ =
  ignore Lwt.(
      catch begin fun () ->
        debug "—→ START !!!";
        let client = Client_state.create () in
        attach_to_page H5.(
            div [
              pcdata "HELLO WORLD";
              Client_state.render client;
            ]
          )
        >>= fun () ->
        debug "→ END !!!";
        return ()
      end (fun exn ->
          Printf.ksprintf
            (fun s -> Firebug.console##error (Js.string s); failwith s)
            "Uncaught Exception: %s" (Printexc.to_string exn)));
  Js._true

(* The “main” is just getting the debug-level form the HTML page and
   attaching the `go` function to `window.onload`. *)
let _ =
  let debug_level =
    try
      Js.Unsafe.get Dom_html.window (Js.string "gui_debug_level")
      |> Js.to_string
      |> int_of_string
    with e ->
      debug "getting window.gui_debug_level: %s" (Printexc.to_string e);
      0
  in
  debug "Not using debug_level (%d) from the template" debug_level;
  Dom_html.window##.onload := Dom_html.handler go
