open build/OCaml
open tools/OMakeLibrary.om
DefineCommandVars()

.PHONY: build-all doc install uninstall


################################################################################
# General Project Information
PROJECT = ocoty
VERSION = 0.0.0+master
DESCRIPTION = Somewhat well desribed web-application

if $(test -e .git)
    # this one works with quotes not the `None` one
    GIT_COMMIT = 'Some "$(shell git rev-parse HEAD)"'
    export
else
    GIT_COMMIT = None
    export

if $(test $(getenv WITH_BISECT, "") = true)
    echo "Using Bisect"
    BISECT_PACKAGE= bisect_ppx.fast
    export
else
    echo "Not using Bisect: " $(getenv WITH_BISECT, "")
    BISECT_PACKAGE=
    export

PURE_LIB_PACKAGES = yojson uri cohttp \
   ppx_deriving_yojson ppx_deriving.show $(BISECT_PACKAGE) react reactiveData

LWT_UNIX_LIB_PACKAGES = $(PURE_LIB_PACKAGES) threads cohttp.lwt lwt.react

JOO_PACKAGES = $(PURE_LIB_PACKAGES) js_of_ocaml js_of_ocaml.ppx js_of_ocaml.tyxml

if $(test $(getenv JSOO_DEBUG_MODE, "") = true)
    JSOO_FLAGS=--pretty --no-inline --debug-info
    export
else
    JSOO_FLAGS=
    export

OCAMLFLAGS = -bin-annot -thread -short-paths -g \
             -strict-formats -strict-sequence -w +9 -safe-string

section # Pure library
    OCAMLPACKS[] = $(PURE_LIB_PACKAGES)
    _build/src/lib/metadata.ml: :value: $(VERSION)
        echo "(** Metadata generated at compile-time *)" > $@
        echo "let version = \"$(VERSION)\"" >> $@
        echo "let git_commit = $(GIT_COMMIT)" >> $@
    OCAML_GENERATED_MODULES[] = metadata
    .DEFAULT: $(OCamlPackedLibraryOfDir $(PROJECT), src/lib)


section # Javascript client code
    OCAMLPACKS[] = $(JOO_PACKAGES)
    OCAML_LOCAL_LIBS[] = src/lib/$(PROJECT)
    NATIVE_ENABLED = false
    _build/client.js: $(file _build/src/client_joo/$(PROJECT)-client-joo.run)
        js_of_ocaml $(JSOO_FLAGS) +weak.js $< -o $@
    CSS_FILE=$(file ./src/css/base.css)
    _build/style.css: $(CSS_FILE)
        echo "Building _build/style.css"
        cp $(CSS_FILE) _build/style.css
    DEBUG_LEVEL_IN_GUI=$(getenv DEBUG_LEVEL_IN_GUI, "0")
    _build/gui-page.html: _build/client.js _build/style.css ./tools/template-gui.sh
        ./tools/template-gui.sh $@ _build/client.js _build/style.css \
            $(DEBUG_LEVEL_IN_GUI) "$(PROJECT): $(DESCRIPTION)" "$(PROJECT)-gui"
    .PHONY: gui-quick
    .DEFAULT: $(OCamlProgramOfDirectory $(PROJECT)-client-joo, $(dir src/client_joo)) _build/gui-page.html
    gui-quick: _build/gui-page.html

section # Big Lwt/Unix library; server
    OCAMLPACKS[] = $(LWT_UNIX_LIB_PACKAGES)
    OCAML_LOCAL_LIBS[] = src/lib/$(PROJECT)
    DESCRIPTION = $(DESCRIPTION) -- Lwt/Unix library
    _build/src/lwt_unix/client_html.ml: _build/gui-page.html
        ocamlify --var-string gui_page _build/gui-page.html --output $@
    OCAML_GENERATED_MODULES[] = client_html
    .DEFAULT: $(OCamlPackedLibraryOfDir $(PROJECT)_lwt_unix, src/lwt_unix)

section # "main" application
    OCAML_LOCAL_LIBS += src/lib/$(PROJECT) src/lwt_unix/$(PROJECT)_lwt_unix
    OCAMLPACKS[] = $(LWT_UNIX_LIB_PACKAGES)
    .DEFAULT: $(OCamlProgramOfDirectory $(PROJECT), $(dir src/app))

DotMerlin(./src, $(LWT_UNIX_LIB_PACKAGES) $(JOO_PACKAGES))

install: install-lib-$(PROJECT) install-lib-$(PROJECT)_lwt_unix install-app-$(PROJECT)
uninstall: uninstall-lib-$(PROJECT) uninstall-lib-$(PROJECT)_lwt_unit uninstall-app-$(PROJECT)


.PHONY: doc test-env bisect-report bisect-clean

DOC_PACKAGES[] =  $(LWT_UNIX_LIB_PACKAGES) $(JOO_PACKAGES)

_test_env:
    mkdir -p _test_env
_test_env/cert.pem: _test_env
    $(shell openssl req -x509 -newkey rsa:2048 -keyout _test_env/key.pem \
              -out _test_env/cert.pem -days 10 -nodes -subj "/CN=test_$(PROJECT)")
test-env: _test_env/cert.pem


bisect-clean:
    rm -rf _report_dir bisect*.out

_report_dir:
    mkdir _report_dir

bisect-report: _report_dir
    bisect-ppx-report -I _build/src/lib/ \
                      -I _build/src/app/ -I _build/src/lwt_unit/  \
                      -verbose -html _report_dir  bisect*.out

DIRS[] = src/lib src/lwt_unix
LIBS[] = $(PROJECT) $(PROJECT)_lwt_unix
DotOCamlinit($(DIRS), $(LWT_UNIX_LIB_PACKAGES), $(LIBS))
