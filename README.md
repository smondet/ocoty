Ocoty
=====

Omake, Cohttp, Tyxml_js.

Build & Run
-----------

To build:

    omake

To run:

    ./ocoty 4242

and visit <http://localhost:4242>.

To try SSL (depending on compilation options of `conduit`):

    omake test-env # Create a self-signed ceritificate

then

    ./ocoty 4444 _test_env/cert.pem _test_env/key.pem

or

    CONDUIT_TLS=native ./ocoty 4444 _test_env/cert.pem _test_env/key.pem

to try <https://localhost:4444>.

Make Your Own Project
---------------------

### Change The Name

I've tried to limit the uses of the name of the project:

    git grep -in ocoty

should be easy to start somewhere else.

### Start Hacking

The `OMakeroot` file is the build system (which uses `tools/OMakeLibrary.om`
from [`smondet/omake-hacks`](https://github.com/smondet/omake-hacks)).
It creates:

- a 1st common library `ocoty` (from `src/lib`),
- the client `js_of_ocaml` executable (from `src/client_joo/`),
- a 2nd library `ocoty_lwt_unix` (from `src/lwt_unix/`) that “embeds” the
  resulting javascript and `src/css/base.css` (assembled thanks to
  `tools/template-gui.sh`),
- an executable `./ocoty` (form `src/app/`) that does minimal command line
  parsing and calls `Ocoty_lwt_unix.Server.start`.

(there also installation commands and goodies like `omake .merlin` or `.omake
.ocamlinit`).


